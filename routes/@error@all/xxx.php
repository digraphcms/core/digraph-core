<?php
$package['fields.page_name'] = $package['fields.page_title'] = 'Unknown error';

$package->saveLog('Unknown HTTP status', Digraph\Logging\LogHelper::CRITICAL);
